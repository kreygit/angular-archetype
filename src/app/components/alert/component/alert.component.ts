import { Component, OnInit, Inject } from '@angular/core';

import { Alert } from '../alert.model';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material/snack-bar';
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mat-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public data: Alert,
    private snackRef: MatSnackBarRef<AlertComponent>,
  ) { }

  ngOnInit(): void {
  }

  close(): void {
    this.snackRef.dismiss();
  }
}
