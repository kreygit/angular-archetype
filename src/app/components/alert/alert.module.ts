import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertComponent } from './component/alert.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';



@NgModule({
  declarations: [
    AlertComponent,
  ],
  imports: [
    CommonModule,
    MatSnackBarModule,
    MatIconModule,
  ]
})
export class AlertModule { }
