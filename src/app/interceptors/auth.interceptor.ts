import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    // Créer une méthode qui va récupérer le token
    token = 'token';

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<unknown>> {
        const cloneReq = req.clone({
            setHeaders: { Authorization: this.token}
        });
        return next.handle(cloneReq);
    }
}
