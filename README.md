# Angular Archetype

**Angular archetype** est un archetype basé sur Angular	11.0.0.

Cet archetype comprend :
* Une arborescence classique d'un projet Angular
* La bibiothèque de composant *Angular Material*
* Un thème personnalisé
* Un composant personnalisé d'Alert (inspiré de Bootstrap)
* L'import des icons *Material* en local
* L'import de la *Font* de base en local
* Création d'un module pour l'importation des composants *Angular Material*
* Ajout des profils d'environement *Dev* et *Qualif*
* Ajout d'un fichier comprenant les variables constantes de l'application
* Ajout d'intercepteurs pour les *JWT* et les codes HTML d'erreurs

## Documentation

La documentation des éléments ajoutés et leurs utilisations sont dans la section [Wiki](https://gitlab.com/kreygit/angular-archetype/-/wikis/home) du dépôt.

## Documentation officiel Angular

[Angular](https://angular.io/docs)  
[Angular Material](https://material.angular.io/components/categories)  
[Material Icons](https://material.io/resources/icons/?style=baseline)  
[Angular Github](https://github.com/angular)  
