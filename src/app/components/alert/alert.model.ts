import { MatSnackBarConfig } from '@angular/material/snack-bar';

export interface Alert {
    type: 'primary' | 'accent' | 'warn' | 'success' | 'danger' | 'info' | 'light' | 'dark';
    message: string[];
    autoClose: boolean;
    horizontalPosition: 'start' | 'center' | 'end' | 'left' | 'right';
    verticalPosition: 'top' | 'bottom';
}

export interface AlertConfig extends MatSnackBarConfig {
    data: Alert;
  }

