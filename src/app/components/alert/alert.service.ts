import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarRef } from '@angular/material/snack-bar';
import { AlertConfig } from './alert.model';
import { AlertComponent } from './component/alert.component';

@Injectable({
  providedIn: 'root',
})
export class AlertService {

  constructor(
    private sb: MatSnackBar
  ) { }

  open(config: AlertConfig): MatSnackBarRef<AlertComponent> {
    config.panelClass = 'mat-' + config.data.type;
    config.duration = config.data.autoClose ? 3000 : -1;
    config.horizontalPosition = config.data.horizontalPosition;
    config.verticalPosition = config.data.verticalPosition;
    return this.sb.openFromComponent(AlertComponent, config);
  }
}
