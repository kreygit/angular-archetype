import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, EMPTY } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { AlertConfig } from '../components/alert/alert.model';
import { AlertService } from '../components/alert/alert.service';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

    constructor(
        private alertService: AlertService,
    ) {}
    intercept(
        req: HttpRequest<unknown>,
        next: HttpHandler
    ): Observable<HttpEvent<unknown>> {
        return next.handle(req)
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    const messages: string[] = [];
                    messages.push(error.error.message);
                    const alert: AlertConfig = {
                        data: {
                            message: messages,
                            type: 'warn',
                            autoClose: true,
                            horizontalPosition: 'center',
                            verticalPosition: 'bottom',
                        }
                     };
                    switch (error.status) {
                        case 500:
                        case 501:
                        case 502:
                        case 503:
                        case 504:
                            this.alertService.open(alert);
                            break;
                        case 400:
                        case 401:
                        case 403:
                        case 404:
                        case 405:
                            alert.data.type = 'danger';
                            this.alertService.open(alert);
                            break;
                    }
                    return EMPTY;
                })
            );
    }
}
